/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.campanabase.services;

import com.tdx.campaigntools.objects.SMSRequest;
import static com.tdx.campanabase.config.ApplicationConfig.jsonSenderCampaignData;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author jalfonzo
 */
public class RestInvocationCestaTicket {

    static boolean debugMode = false;

    public static void printDebugMessage(String message) {
        if (debugMode) {
            printMessage(message);
        }
    }

    public static void printMessage(String message) {
        System.out.println("Thread:" + Thread.currentThread().getId() + " CestaTicket: " + message);
    }

    public static boolean validarMensaje(String msg, String regex) {
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(msg);
        return matcher.find();
    }

    public static String getTocken(String endpoint, String metodo, String username, String pass)
            throws MalformedURLException, IOException, JSONException {
        String tocken = "";
        boolean isHttps = endpoint.toLowerCase().startsWith("https");
        HttpsURLConnection httpsConnection = null;
        if (isHttps) {
            URL url = new URL(endpoint);
            httpsConnection = (HttpsURLConnection) url.openConnection();
            httpsConnection.setHostnameVerifier(new TrustAllHosts());
        }
        httpsConnection.setDoOutput(true);
        httpsConnection.setRequestMethod(metodo);
        httpsConnection.setRequestProperty("Content-Type", "application/json");
        httpsConnection.setRequestProperty("Accept", "application/json");
        System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1");
        String input = "{\"Username\":\"" + username + "\",\"Password\":\"" + pass + "\"}";
        printDebugMessage("JSON: " + input);
        OutputStream os = httpsConnection.getOutputStream();
        os.write(input.getBytes());
        os.flush();
        StringBuilder stringBuilder = new StringBuilder();
        if (httpsConnection.getResponseCode() != 200) {
            InputStreamReader streamReader = new InputStreamReader(httpsConnection.getErrorStream());
            try (BufferedReader bufferedReader = new BufferedReader(streamReader)) {
                String response = null;
                while ((response = bufferedReader.readLine()) != null) {
                    stringBuilder.append(String.valueOf(response)).append("\n");
                }
            }
            printMessage(String.valueOf(httpsConnection.getResponseMessage()) + " - " + httpsConnection.getResponseCode() + " / " + stringBuilder.toString());
            tocken = "000000";
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));
            String output;
            while ((output = br.readLine()) != null) {
                stringBuilder.append(output);
            }
            JSONObject jsonObj = new JSONObject(stringBuilder.toString());
            tocken = jsonObj.get("keyToken").toString();
        }
        return tocken;
    }

    public static String process(HashMap<String, String> mp, HashMap<String, String> mr, SMSRequest request) throws Exception {
        String result = "";
        String number = request.getNumero();

        try {
            String debugProperty = mp.get("debug");
            if (debugProperty != null && debugProperty.equalsIgnoreCase("true")) {
                debugMode = true;
            }
            String msg_test = request.getTexto().toUpperCase().trim();
            msg_test = msg_test.replaceAll(" +", " ");
            String mensajeRespuesta = "";
            printDebugMessage("ValidEndpoind :" + (String) mp.get("ValidEndpoind"));
            printDebugMessage("ValidMethod : " + (String) mp.get("ValidMethod"));
            printDebugMessage("numRem: " + number);
            printDebugMessage("mensaje: " + msg_test);
            printDebugMessage("nroEmisor: " + number);
            printDebugMessage("idMensaje: " + request.getIdMensaje());
            String validTocken = getTocken((String) mp.get("ValidEndpoind"), (String) mp.get("ValidMethod"), (String) mp.get("username"), (String) mp.get("clave"));
            printDebugMessage("Token Obtenido: " + validTocken);
            if ("000000".equals(validTocken)) {
                return result = jsonSenderCampaignData.error_msg;
            }
            //SEND REQUEST
            boolean isHttps = ((String) mp.get("endpoint")).toLowerCase().startsWith("https");
            HttpsURLConnection httpsConnection = null;
            if (isHttps) {
                URL url = new URL((String) mp.get("endpoint"));
                httpsConnection = (HttpsURLConnection) url.openConnection();
                httpsConnection.setHostnameVerifier(new TrustAllHosts());
            }
            httpsConnection.setDoOutput(true);
            httpsConnection.setRequestMethod((String) mp.get("method"));
            httpsConnection.setRequestProperty("Content-Type", "application/json");
            httpsConnection.setRequestProperty("Accept", "application/json");
            httpsConnection.setRequestProperty("Authorization", "Bearer " + validTocken);
            String input = "{\"idMensaje\":\"" + request.getIdMensaje() + "\",\"codPais\":\"" + request.getNumero().substring(0, 2) + "\",\"codArea\":\"" + request.getNumero().substring(2, 5) + "\",\"numRem\":\"" + request.getNumero().substring(5) + "\",\"mensaje\":\"" + request.getTexto() + "\"}";
            printDebugMessage("JSON: " + input);
            OutputStream os = httpsConnection.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            StringBuilder stringBuilder = new StringBuilder();

            if (httpsConnection.getResponseCode() != 200) {
                InputStreamReader streamReader = new InputStreamReader(httpsConnection.getErrorStream());
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                String response = null;
                while ((response = bufferedReader.readLine()) != null) {
                    stringBuilder.append(String.valueOf(response)).append("\n");
                }
                bufferedReader.close();
                printMessage(String.valueOf(httpsConnection.getResponseMessage()) + " - " + httpsConnection.getResponseCode() + " / " + stringBuilder.toString());
                return result = jsonSenderCampaignData.error_msg;
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));
                String output;
                while ((output = br.readLine()) != null) {
                    stringBuilder.append(output);
                }
                result = stringBuilder.toString();
            }

            httpsConnection.disconnect();
            if (result.trim().isEmpty()) {
                return result = jsonSenderCampaignData.error_msg;
            }

        } catch (Exception ex) {
            result = jsonSenderCampaignData.error_msg;
            printMessage("ERROR Exception: ");
            ex.printStackTrace();
            return result;
            //
        }
        JSONObject jsonObj = new JSONObject(result);
        result = jsonObj.get("Result").toString();
        return result;
    }
}
